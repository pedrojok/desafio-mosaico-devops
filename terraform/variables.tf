variable "region" {
  type        = string
  default     = "us-east-1"
  description = "Região AWS"
}

variable "app_count" {
  type = number
  default = 1
}