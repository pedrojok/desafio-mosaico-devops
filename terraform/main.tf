### Data para buscar as AZs da Região definida no provides. ###

data "aws_availability_zones" "available_zones" {
  state = "available"
}

### Criação da VPC e componetes ###

resource "aws_vpc" "vpc-desafio" {
  cidr_block       = "10.23.0.0/16"

  tags = {
    Name = "vpc-desafio"
  }
}

### Criação das Subnetes - 2 Priv e 2 Pub ###

resource "aws_subnet" "public" {
  count                   = 2
  cidr_block              = cidrsubnet(aws_vpc.vpc-desafio.cidr_block, 8, 2 + count.index)
  availability_zone       = data.aws_availability_zones.available_zones.names[count.index]
  vpc_id                  = aws_vpc.vpc-desafio.id
  map_public_ip_on_launch = true
}

resource "aws_subnet" "private" {
  count             = 2
  cidr_block        = cidrsubnet(aws_vpc.vpc-desafio.cidr_block, 8, count.index)
  availability_zone = data.aws_availability_zones.available_zones.names[count.index]
  vpc_id            = aws_vpc.vpc-desafio.id
}

### Criação de Internet GW, NAT GW e Route Tables ###

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.vpc-desafio.id
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.vpc-desafio.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gateway.id
}

resource "aws_eip" "gateway" {
  count      = 2
  vpc        = true
  depends_on = [aws_internet_gateway.gateway]
}

resource "aws_nat_gateway" "gateway" {
  count         = 2
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  allocation_id = element(aws_eip.gateway.*.id, count.index)
}

resource "aws_route_table" "private" {
  count  = 2
  vpc_id = aws_vpc.vpc-desafio.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = element(aws_nat_gateway.gateway.*.id, count.index)
  }
}

resource "aws_route_table_association" "private" {
  count          = 2
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

### Criacao do Security Group para o ALB ###

resource "aws_security_group" "sg-lb" {
  name        = "alb-sg"
  vpc_id      = aws_vpc.vpc-desafio.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

### Criação do ALB, Target Group e do Listen para ser usando no service do ECS. ####

resource "aws_lb" "alb-desafio" {
  name            = "alb-desafio"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.sg-lb.id]
}

resource "aws_lb_target_group" "tg-desafio" {
  name        = "tg-desafio"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc-desafio.id
  target_type = "ip"
}

resource "aws_lb_listener" "listener-desafio" {
  load_balancer_arn = aws_lb.alb-desafio.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.tg-desafio.id
    type             = "forward"
  }
}

### Criação do ECR para armazenamento das imsgens Docker ####

resource "aws_ecr_repository" "ecr-hello-word" {
  name                 = "ecr-hello-word"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

### Criação da taks definition do app ###

resource "aws_ecs_task_definition" "hello_world" {
  family                   = "td-hello-world-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512

  container_definitions = <<DEFINITION
[
  {
    "image": "httpd:latest",
    "cpu": 256,
    "memory": 512,
    "name": "hello-world-app",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": 80,
        "protocol": "tcp",
        "hostPort": 80
      }
    ]
  }
]
DEFINITION
}

### Criação do Security Group para a aplicação ####

resource "aws_security_group" "sg-app-hello_world" {
  name        = "task-security-group"
  vpc_id      = aws_vpc.vpc-desafio.id

  ingress {
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    security_groups = [aws_security_group.sg-lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

### Criação do Cluster ECS e do Service da APP Hello Word ###

resource "aws_ecs_cluster" "ecs-cluster-hello-word" {
  name = "ecs-cluster-hello-word"
}

resource "aws_ecs_service" "hello_world" {
  name            = "hello-world-service"
  cluster         = aws_ecs_cluster.ecs-cluster-hello-word.id
  task_definition = aws_ecs_task_definition.hello_world.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.sg-app-hello_world.id]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.tg-desafio.id
    container_name   = "hello-world-app"
    container_port   = 80
  }

  depends_on = [aws_lb_listener.listener-desafio]
}