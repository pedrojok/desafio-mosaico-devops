
output "load_balancer_ip" {
  value = aws_lb.alb-desafio.dns_name
}

output "repository_url" {
  value = aws_ecr_repository.ecr-hello-word.repository_url
}